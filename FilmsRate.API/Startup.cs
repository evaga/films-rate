﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Startup.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FilmsRate.API;

using Microsoft.Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace FilmsRate.API
{
    using System;
    using System.Diagnostics;
    using System.Web.Http;

    using FilmsRate.API.Authorization;
    using FilmsRate.DAL.Migrations;

    using Microsoft.Owin.Security.OAuth;

    using Owin;

    /// <summary>
    /// The startup.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// The configuration.
        /// </summary>
        /// <param name="app">
        /// The app.
        /// </param>
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            this.ConfigureOAuth(app);
            
            WebApiConfig.Register(config);
            app.UseWebApi(config);

            this.SetupDatabase();
        }

        /// <summary>
        /// Configures <code>oAuth</code> authentication.
        /// </summary>
        /// <param name="app">
        /// The application.
        /// </param>
        public void ConfigureOAuth(IAppBuilder app)
        {
            var oAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new SimpleAuthorizationServerProvider()
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        /// <summary>
        /// Setups the database.
        /// </summary>
        private void SetupDatabase()
        {
            try
            {
                CustomMigration.CreateUsersAndRoles();
            }
            catch (Exception ex)
            {
#if DEBUG
                Trace.TraceError($"Exception encountered: {ex.Message} {ex}");
#endif                
                throw;
            }
        }
    }
}