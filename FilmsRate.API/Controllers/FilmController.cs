﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FilmController.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.API.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;

    using FilmsRate.DAL.Data.Film;
    using FilmsRate.DAL.Data.KpSettings;
    using FilmsRate.DAL.Data.WatchedFilm;
    using FilmsRate.KinopoiskGrabber;

    /// <summary>
    /// The film controller.
    /// </summary>
    [RoutePrefix("api/Film")]
    public class FilmController : BaseController
    {
        /// <summary>
        /// Gets or sets the films repository.
        /// </summary>
        private FilmsRepository FilmsRepo { get; set; }

        /// <summary>
        /// Gets or sets the watched films repository.
        /// </summary>
        private WatchedFilmsRepository WatchedFilmsRepo { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FilmController"/> class.
        /// </summary>
        public FilmController()
        {
            this.FilmsRepo = new FilmsRepository();
            this.WatchedFilmsRepo = new WatchedFilmsRepository();
        }

        /// <summary>
        /// Gets watched films
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{T}"/> of watched films.
        /// </returns>
        [Route("Watched")]
        [Authorize(Roles = "User")]
        public async Task<IHttpActionResult> GetWatched()
        {
            IQueryable<WatchedFilm> watchedFilms = null;
            await this.ExecuteForCurrentUser(currentUser => watchedFilms = this.WatchedFilmsRepo.GetByUserId(currentUser.Id));

            return this.Ok(watchedFilms.ToList().Select(watchedFilm => { watchedFilm.Film.Rating = watchedFilm.Rating; return watchedFilm.Film; }));
        }

        /// <summary>
        /// Gets unwatched films
        /// </summary>
        /// <param name="count">
        /// The count of films to be returned.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{T}"/> of unwatched films.
        /// </returns>
        [Route("Unwatched")]
        [Authorize(Roles = "User")]
        public async Task<IHttpActionResult> GetUnwatched(int count)
        {
            IEnumerable<Film> unwatchedFilms = null;
            await this.ExecuteForCurrentUser(
                currentUser =>
                {
                    unwatchedFilms = this.FilmsRepo.GetUnwatchedByUserId(currentUser.Id, count).ToList();
                    this.UpdateKpFilmInfo(unwatchedFilms, currentUser.Id);
                });

            return this.Ok(unwatchedFilms);
        }

        /// <summary>
        /// Gets the comparable films pair.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{T}"/> with comparable films pair.
        /// </returns>
        [Route("Comparable")]
        [Authorize(Roles = "User")]
        public async Task<IHttpActionResult> GetComparablePair()
        {
            IEnumerable<Film> pair = null;
            await this.ExecuteForCurrentUser(
                currentUser =>
                {
                    pair = this.WatchedFilmsRepo.GetPairByUserId(currentUser.Id).Select(wf => wf.Film).ToList();
                    this.UpdateKpFilmInfo(pair, currentUser.Id);
                });
            
            return this.Ok(pair);
        }

        /// <summary>
        /// Marks film with specified id as watched and returns the unwatched one to replace watched.
        /// </summary>
        /// <param name="filmId">
        /// The film watched id.
        /// </param>
        /// <param name="unwatched0">
        /// The first unwatched film id which shouldn't be returned as unwatched.
        /// </param>
        /// <param name="unwatched1">
        /// The second unwatched film id which shouldn't be returned as unwatched.
        /// </param>
        /// <returns>
        /// The unwatched <see cref="Film"/>.
        /// </returns>
        [HttpGet]
        [Route("Addwatched")]
        [Authorize(Roles = "User")]
        public async Task<IHttpActionResult> AddWatchedFilm(int filmId, int unwatched0 = 0, int unwatched1 = 0)
        {
            IEnumerable<Film> unwatchedFilms = null;
            await this.ExecuteForCurrentUser(
                currentUser =>
                {
                    this.WatchedFilmsRepo.Create(new WatchedFilm { FilmId = filmId, UserId = currentUser.Id });
                    this.WatchedFilmsRepo.Save();

                    unwatchedFilms = this.FilmsRepo.GetUnwatchedByUserId(currentUser.Id, 1, unwatched0, unwatched1).ToList();
                    this.UpdateKpFilmInfo(unwatchedFilms, currentUser.Id);
                });
            
            return this.Ok(unwatchedFilms.FirstOrDefault());
        }

        /// <summary>
        /// Updates the films rating based on user vote.
        /// </summary>
        /// <param name="votedFilmId">
        /// The film which is a winner of the voting.
        /// </param>
        /// <param name="unvotedFilmId">
        /// The film which is a loser of the voting.
        /// </param>
        /// <param name="equal">
        /// The value indicates there is no winner in the voting.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{T}"/> with new comparable films pair.
        /// </returns>
        [HttpGet]
        [Route("Vote")]
        [Authorize(Roles = "User")]
        public async Task<IHttpActionResult> VoteForFilm(int votedFilmId, int unvotedFilmId, bool equal)
        {
            IEnumerable<Film> pair = null;
            await this.ExecuteForCurrentUser(
                currentUser =>
                {
                    pair = this.WatchedFilmsRepo.GetPairByUserId(currentUser.Id).Select(wf => wf.Film).ToList();

                    var votedFilm = this.WatchedFilmsRepo.GetByFilmId(votedFilmId).FirstOrDefault(film => film.UserId == currentUser.Id);
                    var unvotedFilm = this.WatchedFilmsRepo.GetByFilmId(unvotedFilmId).FirstOrDefault(film => film.UserId == currentUser.Id);
                    if ((votedFilm == null) || (unvotedFilm == null))
                    {
                        throw new Exception(@"Couldn't get watched film from database");
                    }

                    var eVoted = 1 / (1 + Math.Pow(10, (float)(unvotedFilm.Rating - votedFilm.Rating) / 400));
                    var eUnvoted = 1 / (1 + Math.Pow(10, (float)(votedFilm.Rating - unvotedFilm.Rating) / 400));

                    var sVoted = equal ? 0.5 : 1;
                    var sUnvoted = equal ? 0.5 : 0;

                    var kVoted = votedFilm.Rating >= 2400 ? 10 : (votedFilm.Rating >= 2300 ? 20 : 40);
                    var kUnvoted = unvotedFilm.Rating >= 2400 ? 10 : (unvotedFilm.Rating >= 2300 ? 20 : 40);

                    votedFilm.Rating = votedFilm.Rating + (int)Math.Round(kVoted * (sVoted - eVoted));
                    unvotedFilm.Rating = unvotedFilm.Rating + (int)Math.Round(kUnvoted * (sUnvoted - eUnvoted));

                    this.WatchedFilmsRepo.Update(votedFilm);
                    this.WatchedFilmsRepo.Update(unvotedFilm);
                    this.WatchedFilmsRepo.Save();

                    this.UpdateKpFilmInfo(pair, currentUser.Id);
                });
            
            return this.Ok(pair);
        }

        /// <summary>
        /// Updates KP film info.
        /// </summary>
        /// <param name="films">
        /// The films to be updated.
        /// </param>
        /// <param name="userId">
        /// The current user id.
        /// </param>
        private void UpdateKpFilmInfo(IEnumerable<Film> films, int userId)
        {
            using (var kpRepo = new KpSettingsRepository())
            {
                var kpGrabber = new KPGrabber(kpRepo.GetSettingsOrCreateDefault(userId));
                foreach (var film in films)
                {
                    if (!film.KpLoaded)
                    {
                        kpGrabber.UpdateFilmWithKpInfo(film);
                    }
                }
            }
        }
    }
}