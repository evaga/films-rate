﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccountController.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.API.Controllers
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using System.Web.Http;

    using FilmsRate.DAL.Authentication;
    using FilmsRate.DAL.Data.User;

    /// <summary>
    /// The base controller.
    /// </summary>
    public class BaseController : ApiController
    {
        /// <summary>
        /// Executes specified action with user who initialized controller.
        /// </summary>
        /// <param name="action">The <see cref="Action"/> to be executed</param>
        protected virtual async Task ExecuteForCurrentUser(Action<User> action)
        {
            try
            {
                User currentUser;
                using (var authRepo = new AuthRepository())
                {
                    currentUser = await authRepo.GetUserByIdentityName(this.RequestContext.Principal.Identity.Name);
                }

                action(currentUser);
            }
            catch (Exception ex)
            {
#if DEBUG
                Trace.TraceError($"Exception encountered: {ex.Message} {ex}");
#endif                
                throw;
            }
        }
    }
}