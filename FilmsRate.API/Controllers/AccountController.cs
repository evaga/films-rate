﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccountController.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.API.Controllers
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Results;

    using Microsoft.AspNet.Identity;

    using FilmsRate.API.Models;
    using FilmsRate.DAL.Authentication;
    using FilmsRate.DAL.Data.User;

    /// <summary>
    /// The account controller.
    /// </summary>
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        /// <summary>
        /// The repository.
        /// </summary>
        private IAuthRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// </summary>
        public AccountController()
        {
            this.repo = new AuthRepository();
        }

        /// <remarks>
        /// POST api/Account/Register
        /// </remarks>
        /// <summary>
        /// Registers user.
        /// </summary>
        /// <param name="userModel">
        /// The user model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(UserModel userModel)
        {
            try
            {
                if (!this.ModelState.IsValid)
                {
#if DEBUG
                    foreach (var key in this.ModelState.Keys)
                    {
                        Trace.TraceInformation(key + " | " + this.ModelState[key].Value + " | " + this.ModelState[key].Errors.Select(e => e.ErrorMessage).Aggregate((s1, s2) => s1 + " " + s2));
                    }
#endif
                    return this.BadRequest(this.ModelState);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Trace.TraceError($"Exception encountered: {ex.Message} {ex.ToString()}");
#endif
            }

            var user = new User
                           {
                               Email = userModel.UserName,
                               FirstName = userModel.FirstName,
                               LastName = userModel.LastName,
                               RegistartionDate = DateTime.UtcNow
                           };

            IHttpActionResult errorResult;
            try
            {
                var result = await this.repo.RegisterUser(userModel.UserName, userModel.Password, user);
                errorResult = this.GetErrorResult(result);
            }
            catch (Exception ex)
            {
                errorResult = new ExceptionResult(ex, this);
            }

            if (errorResult != null)
            {
                return errorResult;
            }

            return this.Ok();
        }

        /// <summary>
        /// Disposes the object.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.repo.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Gets the error result.
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        /// <returns>
        /// The <see cref="IHttpActionResult"/>.
        /// </returns>
        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return this.InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        this.ModelState.AddModelError(string.Empty, error);
                    }
                }

                if (this.ModelState.IsValid)
                {
                    return this.BadRequest();
                }

                return this.BadRequest(this.ModelState);
            }

            return null;
        }
    }
}