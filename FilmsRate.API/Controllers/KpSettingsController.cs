﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KpSettingsController.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.API.Controllers
{
    using System;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using FilmsRate.DAL.Data.KpSettings;

    [RoutePrefix("api/KpSettings")]
    public class KpSettingsController : BaseController
    {
        /// <summary>
        /// Gets or sets the KP settings repository.
        /// </summary>
        private KpSettingsRepository KpSettingsRepository { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="KpSettingsController"/> class.
        /// </summary>
        public KpSettingsController()
        {
            this.KpSettingsRepository = new KpSettingsRepository();
        }

        /// <summary>
        /// Gets the settings.
        /// </summary>
        /// <returns>
        /// The <see cref="KpSettings"/> for current user.
        /// </returns>
        [HttpGet]
        [Route("Get")]
        [Authorize(Roles = "User")]
        public async Task<IHttpActionResult> GetSettings()
        {
            KpSettings settings = null;
            await this.ExecuteForCurrentUser(currentUser => settings = this.KpSettingsRepository.GetSettingsOrCreateDefault(currentUser.Id));

            return this.Ok(settings);
        }

        /// <summary>
        /// Checks if the current user is able to update KP settings.
        /// </summary>
        /// <returns>
        /// The value indicating whether the current user is able to update KP settings.
        /// </returns>
        [HttpGet]
        [Route("CanUpdate")]
        [Authorize(Roles = "User")]
        public bool CanUpdate()
        {
            bool canUpdate;
            try
            {
                canUpdate = this.User.IsInRole("Admin");
            }
            catch (Exception ex)
            {
#if DEBUG
                Trace.TraceError($"Exception encountered: {ex.Message} {ex}");
#endif                
                throw;
            }
            return canUpdate;
        }

        /// <summary>
        /// Updates the KP settings.
        /// </summary>
        /// <param name="uuid">The UUID to be updated.</param>
        /// <param name="phpsessid">The PHPSESSID to be updated.</param>
        /// <returns>The status</returns>
        [HttpGet]
        [Route("Update")]
        [Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> UpdateSettings(string uuid, string phpsessid)
        {
            await this.ExecuteForCurrentUser(
                currentUser =>
                    {
                        var settings = this.KpSettingsRepository.GetSettingsOrCreateDefault(currentUser.Id);
                        settings.Uuid = uuid;
                        settings.Phpsessid = phpsessid;

                        this.KpSettingsRepository.Update(settings);
                        this.KpSettingsRepository.Save();
                    });

            return this.Ok();
        }

        /// <summary>
        /// Uploads the KP export file and then updates the database according it.
        /// </summary>
        /// <returns>The status</returns>
        [HttpPost, Route("UploadKpExport")]
        [Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> UploadKpExport()
        {
            if (!this.Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            KpSettings settings = null;
            await this.ExecuteForCurrentUser(currentUser => settings = this.KpSettingsRepository.GetSettingsOrCreateDefault(currentUser.Id));

            var provider = new MultipartMemoryStreamProvider();
            await this.Request.Content.ReadAsMultipartAsync(provider);

            foreach (var file in provider.Contents)
            {
                var buffer = await file.ReadAsByteArrayAsync();
                KinopoiskGrabber.KPExportLoader.GrabKpIdsBasedOnExportFile(System.Text.Encoding.Default.GetString(buffer), settings);

                break;
            }

            return this.Ok();
        }
    }
}