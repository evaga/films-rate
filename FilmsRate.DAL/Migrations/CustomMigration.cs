﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomMigration.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using Data.User;

    using FilmsRate.DAL.Authentication;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    /// <summary>
    /// The custom migration.
    /// </summary>
    public class CustomMigration
    {
        /// <summary>
        /// The admin name.
        /// </summary>
        private const string AdminName = "root";

        /// <summary>
        /// The admin pass.
        /// </summary>
        private const string AdminPass = "root";

        /// <summary>
        /// The admin email.
        /// </summary>
        private const string AdminEmail = "root@root.com";

        /// <summary>
        /// Creates users and roles.
        /// </summary>
        public static async void CreateUsersAndRoles()
        {
            var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(new AuthContext()));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new AuthContext()));

            foreach (var roleName in Enum.GetNames(typeof(Roles)))
            {
                if (!roleManager.Roles.Any(role => role.Name == roleName))
                {
                    roleManager.Create(new IdentityRole { Name = roleName });
                }
            }

            var authRepo = new AuthRepository();
            if (!await authRepo.IsUserExist(AdminName, AdminPass))
            {
                await authRepo.RegisterUser(AdminName, AdminPass, new User());

                userManager.Create(new IdentityUser { UserName = AdminName, Email = AdminEmail, EmailConfirmed = true }, AdminPass);
                userManager.AddToRoles(userManager.FindByName(AdminName).Id, "SuperAdmin", "Admin", "User");
            }
        }
    }
}
