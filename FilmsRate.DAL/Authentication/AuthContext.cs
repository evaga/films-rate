﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthContext.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Authentication
{
    using Microsoft.AspNet.Identity.EntityFramework;

    /// <summary>
    /// The <code>auth</code> context.
    /// </summary>
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthContext"/> class.
        /// </summary>
        public AuthContext()
            : base("AuthContext")
        {
        }
    }
}
