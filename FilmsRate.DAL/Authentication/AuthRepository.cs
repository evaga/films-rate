﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthRepository.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Authentication
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    using FilmsRate.DAL.Data.User;

    /// <summary>
    /// The <code>auth</code> repository.
    /// </summary>
    public class AuthRepository : IAuthRepository
    {
        /// <summary>
        /// Gets or sets the <code>auth</code> Context.
        /// </summary>
        private AuthContext Context { get; set; }

        /// <summary>
        /// Gets or sets the user manager.
        /// </summary>
        private UserManager<IdentityUser> UserManager { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthRepository"/> class.
        /// </summary>
        public AuthRepository()
        {
            try
            {
                this.Context = new AuthContext();
                this.UserManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(this.Context));
            }
            catch (Exception ex)
            {
#if DEBUG
                Trace.TraceError($"Eception encountered: {ex.Message} \nstack: {ex.ToString()}");
#endif
                throw;
            }
        }

        /// <summary>
        /// Registers user.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="userInfo">
        /// The user info
        /// </param>
        /// <returns>
        /// The <see cref="Task"/> with results.
        /// </returns>
        public async Task<IdentityResult> RegisterUser(string userName, string password, User userInfo)
        {
            var user = new IdentityUser
            {
                UserName = userName
            };

            var result = await this.UserManager.CreateAsync(user, password);
            
            if ((result != null) && result.Succeeded)
            {
                this.UserManager.AddToRole(user.Id, Roles.User.ToString());

                var userRepo = new UsersRepository();
                userInfo.IdentityUserId = user.Id;
                userRepo.Create(userInfo);
                userRepo.Save();
            }

            return result;
        }

        /// <summary>
        /// Finds user with credentials.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> value indicating whether the user exists.
        /// </returns>
        public async Task<bool> IsUserExist(string userName, string password)
        {
            return await this.UserManager.FindAsync(userName, password) != null;
        }

        /// <summary>
        /// Gets the user by identity name.
        /// </summary>
        /// <param name="userName">The user identity name.</param>
        /// <returns>The <see cref="User"/> for identity.</returns>
        public async Task<User> GetUserByIdentityName(string userName)
        {
            var user = await this.UserManager.FindByNameAsync(userName);

            using (var repo = new UsersRepository())
            {
                return repo.GetUserByIdentityId(user.Id);
            }
        }

        /// <summary>
        /// Gets users roles
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="authenticationType">The authentication type</param>
        /// <returns>The <see cref="IEnumerable{T}"/> with roles of specified user.</returns>
        public async Task<IEnumerable<string>> GetUserRoles(string userName, string authenticationType)
        {
            var user = await this.UserManager.FindByNameAsync(userName);

            return await this.UserManager.GetRolesAsync(user.Id);
        }

        /// <summary>
        /// Disposes object.
        /// </summary>
        public void Dispose()
        {
            this.Context.Dispose();
            this.UserManager.Dispose();
        }
    }
}
