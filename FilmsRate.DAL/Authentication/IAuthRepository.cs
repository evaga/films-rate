﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAuthRepository.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Authentication
{
    using System;
    using System.Threading.Tasks;

    using FilmsRate.DAL.Data.User;

    using Microsoft.AspNet.Identity;

    /// <summary>
    /// The <code>AuthRepository</code> interface.
    /// </summary>
    public interface IAuthRepository : IDisposable
    {
        /// <summary>
        /// Registers user.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="userInfo">
        /// The user info
        /// </param>
        /// <returns>
        /// The <see cref="Task"/> with results.
        /// </returns>
        Task<IdentityResult> RegisterUser(string userName, string password, User userInfo);

        /// <summary>
        /// Finds user with credentials.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> value indicating whether the user exists.
        /// </returns>
        Task<bool> IsUserExist(string userName, string password);
    }
}
