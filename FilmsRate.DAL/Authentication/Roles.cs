﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Roles.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FilmsRate.DAL.Authentication
{
    /// <summary>
    /// The roles enumeration.
    /// </summary>
    public enum Roles
    {
        /// <summary>
        /// The super admin.
        /// </summary>
        SuperAdmin,

        /// <summary>
        /// The admin.
        /// </summary>
        Admin,

        /// <summary>
        /// The user.
        /// </summary>
        User,

        /// <summary>
        /// The guest.
        /// </summary>
        Guest
    }
}
