﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Variables.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL
{
    /// <summary>
    /// The variables.
    /// </summary>
    public static class Variables
    {
        /// <summary>
        /// The authentication context.
        /// </summary>
        public const string AuthContext = @"AuthContext";

        /// <summary>
        /// The data context.
        /// </summary>
        public const string DataContext = @"DataContext";

        /// <summary>
        /// Dummy code for <code>SqlServer.dll</code> copying.
        /// </summary>
        private static bool instanceExists = System.Data.Entity.SqlServer.SqlProviderServices.Instance != null;
    }
}
