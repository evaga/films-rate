﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataRepository.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Data
{
    using System;
    using System.Data.Entity;
    using System.Diagnostics;
    using System.Linq;

    /// <summary>
    /// The data repository.
    /// </summary>
    /// <typeparam name="T">
    /// The type of entity.
    /// </typeparam>
    public abstract class DataRepository<T> : IDataRepository<T>
        where T : class
    {
        /// <summary>
        /// Gets or sets the data context.
        /// </summary>
        protected DataContext DataContext { get; set; }

        /// <summary>
        /// Gets or sets the data set.
        /// </summary>
        protected DbSet<T> DataSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether object is disposed.
        /// </summary>
        private bool Disposed { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataRepository{T}"/> class.
        /// </summary>
        public DataRepository()
        {
            this.DataContext = new DataContext();

#if DEBUG
            this.DataContext.Database.Log = s =>
                {
                    Trace.TraceInformation($"SQL statement executed:\n{s}");
                };
#endif
        }

        /// <summary>
        /// Gets all objects in data set.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable{T}"/> with objects.
        /// </returns>
        public virtual IQueryable<T> All()
        {
            return this.DataSet;
        }

        /// <summary>
        /// Gets the entity object.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public virtual T Get(int id)
        {
            return this.DataSet.Find(id);
        }

        /// <summary>
        /// Creates  the entity object.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public virtual void Create(T item)
        {
            this.DataSet.Add(item);
        }

        /// <summary>
        /// Updates the entity object.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public virtual void Update(T item)
        {
            this.DataContext.Entry(item).State = EntityState.Modified;
        }

        /// <summary>
        /// Deletes the entity object.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        public virtual void Delete(int id)
        {
            var user = this.DataSet.Find(id);
            if (user != null)
            {
                this.DataSet.Remove(user);
            }
        }

        /// <summary>
        /// Saves the entity object.
        /// </summary>
        public virtual void Save()
        {
            this.DataContext.SaveChanges();
        }

        /// <summary>
        /// Disposes the object.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.Disposed)
            {
                if (disposing)
                {
                    this.DataContext.Dispose();
                }
            }

            this.Disposed = true;
        }

        /// <summary>
        /// Disposes the object.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
