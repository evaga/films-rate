﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Film.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Data.Film
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The film.
    /// </summary>
    public class Film
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the KP id.
        /// </summary>
        public int KpId { get; set; }

        /// <summary>
        /// Gets or sets the KP url.
        /// </summary>
        [StringLength(255)]
        public string KpUrl { get; set; }

        /// <summary>
        /// Gets or sets the genre.
        /// </summary>
        [StringLength(255)]
        public string Genre { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the name in Russian.
        /// </summary>
        [StringLength(255)]
        public string NameRu { get; set; }

        /// <summary>
        /// Gets or sets the name in English.
        /// </summary>
        [StringLength(255)]
        public string NameEn { get; set; }

        /// <summary>
        /// Gets or sets the poster url.
        /// </summary>
        [StringLength(255)]
        public string PosterUrl { get; set; }

        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether KP info is loaded for film.
        /// </summary>
        public bool KpLoaded { get; set; }

        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        [NotMapped]
        public int Rating { get; set; }
    }
}
