﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FilmsRepository.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Data.Film
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FilmsRate.DAL.Data.WatchedFilm;

    /// <summary>
    /// The films repository.
    /// </summary>
    public class FilmsRepository : DataRepository<Film>
    {
        /// <summary>
        /// The random.
        /// </summary>
        private static readonly Random Random = new Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="FilmsRepository"/> class.
        /// </summary>
        public FilmsRepository()
        {
            this.DataSet = this.DataContext.Films;
        }

        /// <summary>
        /// Gets all films.
        /// </summary>
        /// <returns>
        /// The <see cref="IList{T}"/> with films.
        /// </returns>
        public IList<Film> GetAll()
        {
            return this.DataSet.ToList();
        }

        /// <summary>
        /// Get the film by name and year.
        /// </summary>
        /// <param name="nameRu">
        /// The name in Russian.
        /// </param>
        /// <param name="nameEn">
        /// The name in English.
        /// </param>
        /// <param name="year">
        /// The year.
        /// </param>
        /// <returns>
        /// The <see cref="Film"/> with name and year.
        /// </returns>
        public Film GetByNameAndYear(string nameRu, string nameEn, int year)
        {
            return this.DataSet.FirstOrDefault(
                film => (film.NameRu == nameRu) && 
                (film.NameEn == nameEn) && 
                film.Year == year);
        }

        /// <summary>
        /// Gets unwatched films by user id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="count">
        /// The count of films to return.
        /// </param>
        /// <param name="ignored">
        /// The ignored films ids.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{T}"/> with films.
        /// </returns>
        public IEnumerable<Film> GetUnwatchedByUserId(int userId, int count = 3, params int[] ignored)
        {
            var results = new List<Film>();
            using (var watchedFilmsRepo = new WatchedFilmsRepository())
            {
                var watchedFilmIds = watchedFilmsRepo.All().Where(wf => wf.UserId == userId).Select(wf => wf.FilmId).Union(ignored).ToList();
                var allUnwatchedFilms = this.DataSet.Where(f => !watchedFilmIds.Contains(f.Id));

                var availableCount = Math.Min(count, allUnwatchedFilms.Count());
                for (var i = 0; i < availableCount; i++)
                {
                    var ids = results.Where(f => f != null).Select(f => f.Id).ToArray();
                    var skip = (int)(Random.NextDouble() * (allUnwatchedFilms.Count() - results.Count()));
                    results.Add(allUnwatchedFilms.Where(wf => ids.All(id => id != wf.Id)).OrderBy(o => o.Id).Skip(skip).FirstOrDefault());
                }
            }

            return results;
        }
    }
}
