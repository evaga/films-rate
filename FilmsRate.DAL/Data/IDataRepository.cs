﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDataRepository.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Data
{
    using System;
    using System.Linq;

    /// <summary>
    /// The DataRepository interface.
    /// </summary>
    /// <typeparam name="T">
    /// The type of entity.
    /// </typeparam>
    public interface IDataRepository<T> : IDisposable where T : class
    {
        /// <summary>
        /// Gets all objects in data set.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable{T}"/> with objects.
        /// </returns>
        IQueryable<T> All();

        /// <summary>
        /// Gets the entity object.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        T Get(int id);

        /// <summary>
        /// Creates  the entity object.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        void Create(T item);

        /// <summary>
        /// Updates the entity object.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        void Update(T item);

        /// <summary>
        /// Deletes the entity object.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        void Delete(int id);

        /// <summary>
        /// Saves the entity object.
        /// </summary>
        void Save();
    }
}
