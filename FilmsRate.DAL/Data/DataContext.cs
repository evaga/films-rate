﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataContext.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Data
{
    using System.Data.Entity;

    using FilmsRate.DAL;

    /// <summary>
    /// The data context.
    /// </summary>
    public class DataContext : DbContext
    {
        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public DbSet<User.User> Users { get; set; }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public DbSet<Film.Film> Films { get; set; }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public DbSet<WatchedFilm.WatchedFilm> WatchedFilms { get; set; }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public DbSet<KpSettings.KpSettings> KpSettings { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataContext"/> class. 
        /// </summary>
        public DataContext() : base(Variables.DataContext)
        {
        }

        /// <summary>
        /// The on model creating.
        /// </summary>
        /// <param name="modelBuilder">
        /// The model builder.
        /// </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            /*
            modelBuilder.Entity<User.User>()
                        .HasMany<Film.Film>(f => f.WatchedFilms)
                        .WithMany(u => u.WatchedByUsers)
                        .Map(cs =>
                        {
                            cs.MapLeftKey("UserId");
                            cs.MapRightKey("FilmId");
                            cs.ToTable("WatchedFilms");
                        });*/
            /*
            modelBuilder.Entity<Event.Event>()
                        .HasMany<User.User>(e => e.Participants)
                        .WithMany(u => u.JoinedEvents)
                        .Map(cs =>
                        {
                            cs.MapLeftKey("EventRefId");
                            cs.MapRightKey("UserRefId");
                            cs.ToTable("EventParticipant");
                        });*/
        }
    }
}
