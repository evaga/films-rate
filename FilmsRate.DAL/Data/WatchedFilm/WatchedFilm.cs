﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WatchedFilm.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Data.WatchedFilm
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using FilmsRate.DAL.Data.Film;
    using FilmsRate.DAL.Data.User;

    /// <summary>
    /// The watched film.
    /// </summary>
    public class WatchedFilm
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        [Column(Order = 0), Key, ForeignKey("User")]
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the film id.
        /// </summary>
        [Column(Order = 1), Key, ForeignKey("Film")]
        public int FilmId { get; set; }

        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        public int Rating { get; set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Gets or sets the film.
        /// </summary>
        public virtual Film Film { get; set; }
    }
}
