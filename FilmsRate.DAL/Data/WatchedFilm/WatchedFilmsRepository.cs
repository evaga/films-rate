﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WatchedFilmsRepository.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Data.WatchedFilm
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    /// <summary>
    /// The watched films repository.
    /// </summary>
    public class WatchedFilmsRepository : DataRepository<WatchedFilm>
    {
        /// <summary>
        /// The random.
        /// </summary>
        private static readonly Random Random = new Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="WatchedFilmsRepository"/> class.
        /// </summary>
        public WatchedFilmsRepository()
        {
            this.DataSet = this.DataContext.WatchedFilms;
        }

        /// <summary>
        /// Gets the watched films by user id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IQueryable{T}"/> with watched films.
        /// </returns>
        public IQueryable<WatchedFilm> GetByUserId(int id)
        {
            return this.DataSet.Include(x => x.User).Include(x => x.Film).Where(f => f.UserId == id);
        }

        /// <summary>
        /// Gets the watched films by film id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IQueryable{T}"/> with watched films.
        /// </returns>
        public IQueryable<WatchedFilm> GetByFilmId(int id)
        {
            return this.DataSet.Include(x => x.User).Include(x => x.Film).Where(f => f.FilmId == id);
        }

        /// <summary>
        /// Gets the random watched films pair by user id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{T}"/> with watched films.
        /// </returns>
        public IEnumerable<WatchedFilm> GetPairByUserId(int id)
        {
            var watchedFilms = this.DataSet.Include(x => x.User).Include(x => x.Film).Where(f => f.UserId == id);

            var skip = (int)(Random.NextDouble() * watchedFilms.Count());
            var filmOne = watchedFilms.OrderBy(f => f.FilmId).Skip(skip).FirstOrDefault();

            if (filmOne == null)
            {
                return new List<WatchedFilm>();
            }
            
            skip = (int)(Random.NextDouble() * (watchedFilms.Count() - 1));
            var filmTwo = watchedFilms.Where(f => f.FilmId != filmOne.FilmId).OrderBy(f => f.FilmId).Skip(skip).FirstOrDefault();

            if (filmTwo == null)
            {
                return new[] { filmOne };
            }

            return new[] { filmOne, filmTwo };
        }
    }
}
