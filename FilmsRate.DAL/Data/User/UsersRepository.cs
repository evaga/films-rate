﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UsersRepository.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Data.User
{
    using System.Linq;

    /// <summary>
    /// The user repository.
    /// </summary>
    public class UsersRepository : DataRepository<User>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UsersRepository"/> class.
        /// </summary>
        public UsersRepository()
        {
            this.DataSet = this.DataContext.Users;
        }

        /// <summary>
        /// Gets user by identity id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="User"/> with specified id.
        /// </returns>
        public User GetUserByIdentityId(string id)
        {
            return this.DataSet.FirstOrDefault(user => user.IdentityUserId == id);
        }
    }
}
