﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KpSettings.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Data.KpSettings
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The KP settings.
    /// </summary>
    public class KpSettings
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        [Column(Order = 0), Key]
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the UUID.
        /// </summary>
        [StringLength(255)]
        public string Uuid { get; set; }

        /// <summary>
        /// Gets or sets the PHPSESSID.
        /// </summary>
        [StringLength(255)]
        public string Phpsessid { get; set; }
    }
}
