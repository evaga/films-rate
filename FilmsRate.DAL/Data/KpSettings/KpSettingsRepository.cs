﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KpSettingsRepository.cs" company="evagapov">
//   Rate Your Films
// </copyright>
// <summary>
//   The <code>Rate Your Films</code> application API layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FilmsRate.DAL.Data.KpSettings
{
    /// <summary>
    /// The KP settings repository.
    /// </summary>
    public class KpSettingsRepository : DataRepository<KpSettings>
    {
        /// <summary>
        /// The default UUID.
        /// </summary>
        private const string DefaultUuid = @"f60fb8fd919af3f276509700c0081089";

        /// <summary>
        /// The default PHPSESSID.
        /// </summary>
        private const string DefaultPhpsessid = @"51a64a9d76d023b5676dc71ad247e743";

        /// <summary>
        /// Initializes a new instance of the <see cref="KpSettingsRepository"/> class.
        /// </summary>
        public KpSettingsRepository()
        {
            this.DataSet = this.DataContext.KpSettings;
        }

        /// <summary>
        /// Get settings for user or create a default one.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The users <see cref="KpSettings"/>.
        /// </returns>
        public KpSettings GetSettingsOrCreateDefault(int userId)
        {
            var kpSettings = this.Get(userId);
            if (kpSettings == null)
            {
                kpSettings = new KpSettings { UserId = userId, Uuid = DefaultUuid, Phpsessid = DefaultPhpsessid };

                this.Create(kpSettings);
                this.Save();
            }

            return kpSettings;
        }
    }
}
