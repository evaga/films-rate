﻿'use strict';
app.factory('filmrateService', ['$http', function ($http) {
    var serviceBase = 'http://127.0.0.1:8081/';
    var filmrateServiceFactory = {};

    var _getWatchedFilms = function () {

        return $http.get(serviceBase + 'api/film/watched').then(function (results) {
            return results;
        });
    };

    var _getUnwatchedFilms = function (count) {
        var countStmt = count ? "?count=" + count : "";
        return $http.get(serviceBase + 'api/film/unwatched' + countStmt).then(function (results) {
            return results;
        });
    };
    
    var _getComparablePair = function () {
        return $http.get(serviceBase + 'api/film/comparable').then(function (results) {
            return results;
        });
    };

    var _addWatchedAndGetNew = function (filmId, unwatched0, unwatched1) {
        var unwatchedStmt0 = unwatched0 ? "&unwatched0=" + unwatched0 : "";
        var unwatchedStmt1 = unwatched1 ? "&unwatched1=" + unwatched1 : "";
        return $http.get(serviceBase + 'api/film/addwatched?filmId=' + filmId + unwatchedStmt0 + unwatchedStmt1).then(function (results) {
            return results;
        });
    };

    var _voteForFilm = function (votedFilmId, unvotedFilmId, equal) {
        return $http.get(serviceBase + 'api/film/vote?votedFilmId=' + votedFilmId + '&unvotedFilmId=' + unvotedFilmId + '&equal=' + equal).then(function (results) {
            return results;
        });
    };

    var _getKpSettings = function () {
        return $http.get(serviceBase + 'api/kpsettings/get').then(function (results) {
            return results;
        });
    };

    var _updateKpSettings = function (uuid, phpsessid) {
        return $http.get(serviceBase + 'api/kpsettings/update?uuid=' + uuid + '&phpsessid=' + phpsessid).then(function (results) {
            return results;
        });
    };

    var _canUpdateKpSettings = function () {
        return $http.get(serviceBase + 'api/kpsettings/canupdate').then(function (results) {
            return results;
        });
    };

    var _uploadKpExport = function (file) {
        var fd = new FormData();
        fd.append('file', file);

        return $http.post(serviceBase + 'api/kpsettings/uploadkpexport', fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).then(function (results) {
            return results;
        });
    };

    filmrateServiceFactory.getWatchedFilms = _getWatchedFilms;
    filmrateServiceFactory.getUnwatchedFilms = _getUnwatchedFilms;
    filmrateServiceFactory.getComparablePair = _getComparablePair;
    filmrateServiceFactory.addWatchedAndGetNew = _addWatchedAndGetNew;
    filmrateServiceFactory.voteForFilm = _voteForFilm;
    filmrateServiceFactory.getKpSettings = _getKpSettings;
    filmrateServiceFactory.updateKpSettings = _updateKpSettings;
    filmrateServiceFactory.canUpdateKpSettings = _canUpdateKpSettings;
    filmrateServiceFactory.uploadKpExport = _uploadKpExport;

    return filmrateServiceFactory;
}]);