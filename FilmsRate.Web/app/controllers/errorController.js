﻿'use strict';
app.controller('errorController', ['$scope', '$routeParams', function ($scope, $routeParams) {
    $scope.title = "Error";
    $scope.message = "Something went wrong";

    if ($routeParams) {
        if ($routeParams.code === "404") {
            $scope.title = "Error 404";
            $scope.message = "The page you are looking for can't be found";
        }
    }
}]);