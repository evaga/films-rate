﻿'use strict';
app.controller('signupController', ['$scope', '$location', '$timeout', 'authService', function ($scope, $location, $timeout, authService) {
    $scope.savedSuccessfully = false;
    $scope.message = "";

    $scope.registration = {
        userName: "",
        password: "",
        confirmPassword: "",
        firstName: "",
        lastName: "",
        nickName: "",
        sex: "",
        birtDate: ""
    };

    $scope.birthdate = {
        date: null,
        opened: false
    };

    $scope.sexoptions = [
        { label: "Sex", value: -1 },
        { label: "Male", value: 0 },
        { label: "Female", value: 1 }
    ];

    $scope.regions = [
        { label: "Region", value: -1 },
        { label: "Ivanovskaj'a oblast'", value: 0 },
        { label: "Other", value: 1 }
    ];

    $scope.cities = [
        { label: "City", value: -1 },
        { label: "Ivanovo", value: 0 },
        { label: "Other", value: 1 }
    ];

    $scope.sexselected = $scope.sexoptions[0];
    $scope.regionselected = $scope.regions[0];
    $scope.cityselected = $scope.cities[0];

    $scope.dateOptions = {
        "datepicker-mode" : "'year'"
    }

    $scope.openDatepicker = function ($event) {
        $scope.birthdate.opened = true;
    };

    $scope.signUp = function () {

        authService.saveRegistration($scope.registration).then(function (response) {

            $scope.savedSuccessfully = true;
            $scope.message = "User has been registered successfully, you will be redicted to login page in 2 seconds.";
            startTimer();

        },
         function (response) {
             var errors = [];
             for (var key in response.data.modelState) {
                 for (var i = 0; i < response.data.modelState[key].length; i++) {
                     errors.push(response.data.modelState[key][i]);
                 }
             }

             if (response.data.exceptionMessage != null) {
                 errors.push(response.data.exceptionMessage);
             }

             $scope.message = "Failed to register user due to: " + errors.join(' ');
         });
    };

    $scope.sexSelected = function () {
        angular.element(document.querySelector('#sex')).css("color", "#6F6F6F");
    };

    $scope.regionSelected = function () {
        angular.element(document.querySelector('#region')).css("color", "#6F6F6F");
    };

    $scope.citySelected = function () {
        angular.element(document.querySelector('#city')).css("color", "#6F6F6F");
    };

    $scope.showEdit = function () {
        angular.element(document.querySelector('.avatar-edit-icon')).css("display", "block");
    };

    $scope.hideEdit = function () {
        angular.element(document.querySelector('.avatar-edit-icon')).css("display", "none");
    };

    var startTimer = function () {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $location.path('/login');
        }, 2000);
    }
}]);