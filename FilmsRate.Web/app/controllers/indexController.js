﻿'use strict';
app.controller('indexController', ['$scope', '$location', 'authService', function ($scope, $location, authService) {
    $scope.logOut = function () {
        authService.logOut();
        $location.path('/welcome');
    }

    $scope.authentication = authService.authentication;
}]);