﻿'use strict';
app.controller('voteController', ['$scope', 'filmrateService', function ($scope, filmrateService) {
    $scope.watchedfilms = [];

    $scope.addWatched = function (film) {
        var unwatched = [];
        for (var i = 0; i < $scope.unwatchedfilms.length; i++) {
            if ($scope.unwatchedfilms[i].id !== film.id) {
                unwatched.push($scope.unwatchedfilms[i]);
            }
        }

        filmrateService.addWatchedAndGetNew(film.id,
            unwatched.length > 0 ? unwatched[0].id : null,
            unwatched.length > 1 ? unwatched[1].id : null).then(function (results) {
                var updatedUnwatched = [];
                for (var i = 0; i < $scope.unwatchedfilms.length; i++) {
                    if ($scope.unwatchedfilms[i].id === film.id) {
                        if (results.data) {
                            updatedUnwatched.push(results.data);
                        }
                    } else {
                        updatedUnwatched.push($scope.unwatchedfilms[i]);
                    }
                }

                $scope.unwatchedfilms = updatedUnwatched;

                if (!$scope.comparablepair || $scope.comparablepair.length < 2) {
                    $scope.loadComparablePair();
                }
            }, function (error) {
                //alert(error.data.message);
            });
    }

    $scope.vote = function (film) {
        var equal = false;
        if (!film) {
            film = $scope.comparablepair[0];
            equal = true;
        }

        var unvotedFilm = $scope.comparablepair[0].id !== film.id ? $scope.comparablepair[0] : $scope.comparablepair[1];

        filmrateService.voteForFilm(film.id, unvotedFilm.id, equal).then(function (results) {
            $scope.comparablepair = results.data;
        }, function (error) {
            //alert(error.data.message);
        });
    }

    $scope.loadComparablePair = function () {
        filmrateService.getComparablePair().then(function (results) {
            $scope.comparablepair = results.data;
        }, function (error) {
            //alert(error.data.message);
        });
    }

    $scope.loadUnwatchedFilms = function () {
        filmrateService.getUnwatchedFilms(3).then(function (results) {
            $scope.unwatchedfilms = results.data;
        }, function (error) {
            //alert(error.data.message);
        });
    }

    $scope.loadUnwatchedFilms();
    $scope.loadComparablePair();
}]);