﻿'use strict';
app.controller('settingsController', ['$scope', '$interval', 'filmrateService', 'fileService', function ($scope, $interval, filmrateService, fileService) {
    $scope.kpsettings = {
        uuid: "",
        phpsessid: ""
    };

    $scope.settingsmessage = "";
    $scope.canUpdate = false;

    $scope.saveKpSettings = function () {
        filmrateService.updateKpSettings($scope.kpsettings.uuid, $scope.kpsettings.phpsessid).then(function (results) {
            filmrateService.uploadKpExport(fileService[fileService.length - 1]).then(function (results) {
                $scope.settingsmessage = "Changes were saved successfully";
                $scope.settingsSavedSuccessfully = true;
            }, function (error) {
                //alert(error.data.message);
                $scope.settingsmessage = error.error_description;
                $scope.settingsSavedSuccessfully = false;
            });
        }, function (error) {
            //alert(error.data.message);
            $scope.settingsmessage = error.error_description;
            $scope.settingsSavedSuccessfully = false;
        });
    }

    $scope.loadKpSettings = function () {
        filmrateService.getKpSettings().then(function (results) {
            if (results.data) {
                $scope.kpsettings.uuid = results.data.uuid;
                $scope.kpsettings.phpsessid = results.data.phpsessid;
            }
        }, function (error) {
            //alert(error.data.message);
        });
    }

    $scope.canUpdateKpSettings = function () {
        filmrateService.canUpdateKpSettings().then(function (results) {
            if (results.data) {
                $scope.canUpdate = results.data;
            }
        }, function (error) {
            //alert(error.data.message);
        });
    }

    $scope.loadKpSettings();
    $scope.canUpdateKpSettings();

    $interval(function () {
        if (fileService.length > 0) {
            var file = fileService[fileService.length - 1];
            if (file) {
                $scope.exportfilename = file.name;
            }
        }
    }, 100);
}]);