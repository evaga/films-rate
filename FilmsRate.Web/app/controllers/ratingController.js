﻿'use strict';
app.controller('ratingController', ['$scope', 'filmrateService', function ($scope, filmrateService) {
    $scope.watchedfilms = [];
    $scope.scale = 1;
    $scope.offset = 0;

    $scope.loadWatchedFilms = function () {
        filmrateService.getWatchedFilms().then(function (results) {
            $scope.watchedfilms = results.data;

            var maxRating = -10000;
            var minRating = 10000;
            for (var i = 0; i < $scope.watchedfilms.length; i++) {
                if ($scope.watchedfilms[i].rating > maxRating) {
                    maxRating = $scope.watchedfilms[i].rating;
                }

                if ($scope.watchedfilms[i].rating < minRating) {
                    minRating = $scope.watchedfilms[i].rating;
                }
            }

            $scope.offset = 0 - minRating;
            $scope.scale = 10 / (maxRating + $scope.offset);
        }, function (error) {
            //alert(error.data.message);
        });
    }

    $scope.loadWatchedFilms();
}]);