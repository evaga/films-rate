﻿'use strict';
app.directive('fileModel', ['$parse', 'fileService', function ($parse, fileService) {
    return {
        restrict: 'A',
        link: function (scope, element) {
            element.bind('change', function () {
                scope.$apply(function () {
                    if (element[0].files != undefined) {
                        fileService.push(element[0].files[0]);
                    }
                });
            });
        }
    };
}]);