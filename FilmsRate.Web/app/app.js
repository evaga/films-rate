﻿var app = angular.module('FilmsRate', ['ngRoute', 'LocalStorageModule', 'angular-loading-bar', 'ui.bootstrap']);

app.config(function ($routeProvider) {
    $routeProvider.when("/home", {
        controller: "homeController",
        templateUrl: "/app/views/home.html"
    });

    $routeProvider.when("/error", {
        controller: "errorController",
        templateUrl: "/app/views/error.html"
    });

    $routeProvider.when("/welcome", {
        controller: "welcomeController",
        templateUrl: "/app/views/welcome.html"
    });

    $routeProvider.when("/login", {
        controller: "loginController",
        templateUrl: "/app/views/login.html"
    });

    $routeProvider.when("/signup", {
        controller: "signupController",
        templateUrl: "/app/views/signup.html"
    });

    $routeProvider.when("/vote", {
        controller: "voteController",
        templateUrl: "/app/views/vote.html"
    });

    $routeProvider.when("/rating", {
        controller: "ratingController",
        templateUrl: "/app/views/rating.html"
    });

    $routeProvider.when("/settings", {
        controller: "settingsController",
        templateUrl: "/app/views/settings.html"
    });

    $routeProvider.otherwise({
            template: "",
            controller: function ($location) {
                if ($location.$$path === "") {
                    $location.path('/welcome');
                    return false;
                }

                $location.path('/error').search('code', "404");
                return false;
            }
    });
});

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

app.run(['authService', function (authService) {
    authService.fillAuthData();
}]);